import cairo
import argparse
import datetime
import copy

COLWIDTH=255

def show_text_multi(ctx,text):
    lines=text.split("\n")
    linespacing=ctx.text_extents("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")[3]
    x,y=ctx.get_current_point()
    for i,line in enumerate(lines):
        ctx.show_text(line)
        ctx.move_to(x,y+linespacing*(i+1))

def wrap_text(text,ctx,w):
    buff=""
    words=text.split(" ")
    words.reverse()
    lines=[[]]
    while len(words)>0:
        newtext=' '.join(lines[-1]+[words[-1]])
        if ctx.text_extents(newtext)[2]>w:
            lines.append([])
        else:
            lines[-1].append(words.pop())
    return '\n'.join((' '.join(l) for l in lines))
        
            

class Timeline(object):
    def __init__(self,outfile,font="sans-serif",fontsize=12):
        self.outfname=outfile
        self.groups={}
        self.font=font
        self.fontsize=fontsize
        self.start=None
        self.end=None
        self.w=None
        self.h=None
    def append(self,event):
        if self.start==None or event.start<self.start:
            self.start=event.start
        if self.end==None or event.end>self.end:
            self.end=event.end
        if event.group not in self.groups:
            g=Group(event.group)
            self.groups[event.group]=g
        self.groups[event.group].append(event)
    def layout(self,ctx):
        self.w=0
        self.h=0
        maxtexth=0
        for group in self.groups.values():
            group.layout(ctx,self.start)
            if group.texth>maxtexth:
                maxtexth=group.texth
            self.w+=group.w
        maxh=0
        for group in self.groups.values():
            group.texth=maxtexth
            group.rejigger_height()
            if group.h>maxh:
                maxh=group.h
            if group.h>self.h:
                self.h=group.h
        for group in self.groups.values():
            group.h=maxh
    def render(self):
        #dummy surface for doing layout
        surface=cairo.ImageSurface(cairo.FORMAT_ARGB32,1,1)
        ctx=cairo.Context(surface)
        ctx.select_font_face(self.font)
        ctx.set_font_size(self.fontsize)
        self.layout(ctx)
        
        surface=cairo.ImageSurface(cairo.FORMAT_ARGB32,int(self.w+0.5)+255+1,int(self.h+0.5))
        ctx=cairo.Context(surface)
        ctx.select_font_face(self.font)
        ctx.set_font_size(self.fontsize)
        
        now=datetime.datetime.now()
        if now>self.start and now<self.end:
            ctx.save()#draw current-time line
            ctx.set_source_rgb(255,0,0)
            ypos=(now-self.start).total_seconds()/60
            ctx.move_to(0,ypos)
            ctx.line_to(160+self.w,ypos)
            ctx.stroke()
            ctx.restore()
        
        ctx.save()#draw event blocks
        ctx.translate(160,0)
        for group in reversed(self.groups.values()):
            group.render(ctx)
            ctx.translate(group.w,0)
        ctx.restore()
        
        ctx.save()#draw dates
        sdate=copy.copy(self.start)
        ctx.translate(8,self.groups.values()[0].texth+16)
        pday=None
        while sdate<self.end:
            if pday!=sdate.day:
                datestring=sdate.strftime("%H:%M %A %b %d")
                pday=sdate.day
            else:
                datestring=sdate.strftime("%H:%M")
            _,_,w,h,_,_=ctx.text_extents(datestring)
            ctx.move_to(0,h/2)
            ctx.show_text(datestring)
            sdate+=datetime.timedelta(0,0,0,0,0,1,0)#one hour increments
            ctx.translate(0,60)
        ctx.restore()
        
        surface.write_to_png(self.outfname)

class Group(object):
    def __init__(self,name):
        self.name=name
        self.events=[]
        self.w=None
        self.h=None
        self.x=None
        self.y=None
        self.texth=None
    def append(self,event):
        self.events.append(event)
        event.group=self
    def layout(self,ctx,start):
        _,_,w,h,_,_=ctx.text_extents(self.name)
        self.w=COLWIDTH
        self.h=h+16
        self.texth=h
    
        #sort from long to short events
        self.events.sort(key=lambda e:((e.end-e.start).total_seconds(),e.start),reverse=True)
        for event in self.events:
            event.done=False
        while any((e.done==False for e in self.events)):
            for event in self.events:
                if event.done:
                    continue
                event.w+=1
                event.layout(ctx,start)
            for event in self.events:
                if event.done:
                    continue
                px=None
                doundo=False
                while event.x!=px:
                    px=event.x
                    #check for overlap:
                    for other_event in self.events:
                        if event is other_event:
                            continue
                        if event.start>=other_event.end or event.end<=other_event.start:
                            continue#fully outside vertically
                        if event.x+event.w<=other_event.x or event.x>=other_event.x+other_event.w:
                            continue#fully outside horizontally
                        event.x=other_event.x+other_event.w#move right to stop overlap
                    
                if event.y+event.h+self.texth+16>self.h:
                    self.h=event.y+event.h+self.texth+16
                if event.x+event.w+16>self.w:
                    event.done=True
                    #mark all events on the same height as done too.
                    for event in self.events:
                        if event.start>=other_event.end or event.end<=other_event.start:
                            continue#fully outside vertically
                        event.done=True
    def rejigger_height(self):
        for event in self.events:
            if event.y+event.h+self.texth+16>self.h:
                self.h=event.y+event.h+self.texth+16
    def render(self,ctx):
        ctx.save()
        ctx.move_to(0,0)
        ctx.line_to(0,self.h)
        ctx.move_to(self.w,0)
        ctx.line_to(self.w,self.h)
        ctx.set_source_rgb(0,0,0)
        ctx.stroke()
        ctx.move_to(16,8+ctx.text_extents(self.name)[3])
        ctx.show_text(self.name)
        ctx.fill()
        ctx.translate(8,self.texth+16)
        for event in self.events:
            event.render(ctx)
        ctx.restore()

class Event(object):
    def __init__(self,start,end,description,group=""):
        self.start=start
        self.end=end
        self.description=description
        self.group=group
        self.w=0
        self.h=None
        self.x=None
        self.y=None
    def layout(self,ctx,start):
        _,_,w,h,_,_=ctx.text_extents(self.description)
        delta=self.end-self.start
        self.h=delta.total_seconds()/60+2 #1px per minute
        
        delta=self.start-start
        self.y=delta.total_seconds()/60
        self.x=0
    def render(self,ctx):
        ctx.save()
        ctx.translate(self.x+1,self.y+1)
        ctx.move_to(0,0)
        ctx.line_to(self.w-2,0)
        ctx.line_to(self.w-2,self.h-2)
        ctx.line_to(0,self.h-2)
        ctx.close_path()
        ctx.set_source_rgba(1,1,1,0.5)
        ctx.fill_preserve()
        ctx.set_source_rgb(0,0,0)
        ctx.stroke()
        self.description=wrap_text(self.description,ctx,self.w-16)
        ctx.move_to(8,8+ctx.text_extents(self.description)[3])
        show_text_multi(ctx,self.description)
        ctx.fill()
        ctx.restore()
        

if __name__=="__main__":
    parser=argparse.ArgumentParser(description='Generate a fancy-pants timeline from a list of events.')
    parser.add_argument("input",help="text file with a list of events")
    parser.add_argument("output",help="file to put output into")
    
    args=parser.parse_args()
    
    timeline=Timeline(args.output)
    
    with open(args.input,"r") as fid:
        data=fid.read()
    
    group=""
    for line in data.split("\n"):
        if "//" in line:
            line=line.split("//")[0]
        line=line.strip()
        if line=="":
            continue
        
        if line.startswith("###"):
            group=line[3:]
            if line.endswith("###"):
                group=group[:-3]
            group=group.strip()
        else:
            date,what=line.split("#")
            startdate,enddate=date.split("--")
            startdate=datetime.datetime.strptime(startdate.strip(),"%Y-%m-%d %H:%M")
            enddate=datetime.datetime.strptime(enddate.strip(),"%Y-%m-%d %H:%M")
            event=Event(startdate,enddate,what.strip(),group)
            timeline.append(event)
    
    timeline.render()
    print "render done"




